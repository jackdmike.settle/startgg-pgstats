const ATTENDEE_CLASS = "PlayerAvatarCell"
const TABLE_CLASS = "MuiTableBody-root"

const attendeeCollection = document.getElementsByClassName(ATTENDEE_CLASS);

function addButtonToAttendee(attendeeElement) {    
    statsButton = document.createElement("div")
    statsButton.innerHTML="<button class='MuiButton-outlinedPrimary'>PG</button>"
     
    attendeeElement.append(statsButton)
}

var obs = new MutationObserver(function (mutations, observer) {
    //Adds On page Load
    mutations.filter(mutation => mutation.target.className == ATTENDEE_CLASS && mutation.addedNodes.length > 0).forEach(mutation => {
        addButtonToAttendee(mutation.target.childNodes[0].childNodes[0].childNodes[0])
    })

    // Adds on list refresh
    mutations.filter(mutation => mutation.addedNodes.length > 0 && mutation.addedNodes[0].className == ATTENDEE_CLASS).forEach(mutation => {
        addButtonToAttendee(mutation.addedNodes[0].childNodes[0].childNodes[0].childNodes[0])
    })

});

obs.observe(document.getElementsByClassName(TABLE_CLASS)[0], { childList: true, subtree: true, attributes: true, characterData: true });



